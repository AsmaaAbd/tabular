package tabular;

import java.util.LinkedList;
import static java.lang.Math.pow;


public class Essentials {
	int noOfVariables ;
	boolean[][] chart = new boolean[2050][5000];
	LinkedList<String> primes = new LinkedList<String>();						//ABCD   *parallel*
	LinkedList<String> finalExpress = new LinkedList<String>();
	LinkedList donts ;
	
	
	public void chartBuilder(LinkedList<String> primes , int noOfVars , LinkedList donts){				//0-1-dashes
		this.donts = donts ;
		this.noOfVariables = noOfVars ;
		for(int i = 0 ; i < primes.size();i++){
			primesGetter((String)primes.get(i));
			checkChart((String)primes.get(i) , 0 , i);
		}
		minExpression();
		for(int i = 0 ; i < finalExpress.size() ;i++){
			if(i!=finalExpress.size()-1)System.out.print(finalExpress.get(i) + "+");
			else System.out.print(finalExpress.get(i));
		}
	}
	
	public void minExpression(){
		boolean essential = true ;
		for(int i = 0 ; i < primes.size();i++){
			LinkedList record = new LinkedList();
			for(int j = 0 ; j < pow(2,noOfVariables);j++){
				if(chart[i][j]){
					essential = true ;
					record.add(j);
				for(int k = 0 ; k < primes.size();k++){
					if(chart[k][j] && k!=i){essential = false ;}
				}
				if(essential){
					finalExpress.add(primes.get(i));
					break ;
				}
				}
			}
			if(!essential){for(int k = 0 ; k < record.size();k++){
				chart[i][(int)record.get(k)] = false ;
			}}
			essential = true ;
		}
	}
	
	public void primesGetter(String S){
		String express = ""; 
		for(int i = 0 ; i < S.length() ; i++){
			if(S.charAt(i) == '1'){
				char[] c = Character.toChars('A'+i) ;
				express += c[0];
			}
			else if (S.charAt(i) == '0'){
				char[] c = Character.toChars('A'+i) ;
				express += c[0];
				express += '*' ;
			}
		}
		primes.add(express) ;
	}
	
	public void checkChart(String s , int ind , int row){
		if(ind == s.length()){
			int temp = Integer.parseInt(s, 2);
			boolean found = false ;
			for(int i = 0 ; i < donts.size() ; i++){
				if(temp == (int)donts.get(i))found = true ; 
			}
			if(!found)chart[row][ temp ] = true ;
			return ;	
		}
		if(s.charAt(ind)!='x'){
			checkChart(s , ind+1 , row);
			return;
		}
		if(ind != s.length()-1)s = s.substring(0, ind)+'0'+ s.substring(ind+1,s.length());
		else s = s.substring(0, ind)+'0';
		checkChart(s,ind+1, row);
		if(ind != s.length()-1)s = s.substring(0, ind)+'1'+ s.substring(ind+1,s.length());
		else s = s.substring(0, ind)+'1';
		checkChart(s,ind+1,row);
	}


}
